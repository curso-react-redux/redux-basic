import User from "./pages/User"
import Home from "./pages/Home"

const routesConfig = [
    {
        path: "/",
        component:Home,
        exact:true
    },
    {
        path: "/user",
        component:User,
        exact:true
    }
]

export default routesConfig