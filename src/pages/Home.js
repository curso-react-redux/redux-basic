import React, { Component } from 'react'
import logo from '../logo.svg'
import './Home.css'
import { connect } from 'react-redux'

import {
    changeName
} from '../redux/home/homeAction'

class Home extends Component {

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Campo</h1>
                    <input type="text" value={this.props.name} onChange={this.props.changeName} />
                </header>
                <p className="App-intro">
                    Valor do input - {this.props.name}

                </p>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    name: state.homeReducer.name
})

export default connect(mapStateToProps, {changeName})(Home)
